/*
 * Copyright (C) 2016-2020 OX Software GmbH
 * Developed by Peter Höbel peter.hoebel@open-xchange.com
 * See the LICENSE file for licensing conditions
 * SPDX-License-Identifier: MIT
*/

package org.id4me.exceptions;

public class ClientRegistrationSecretExpiredException extends Exception {
	private static final long serialVersionUID = 1L;

	public ClientRegistrationSecretExpiredException(String message) {
		super(message);
	}

}
