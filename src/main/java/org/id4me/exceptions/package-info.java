
/**
 * Exceptions thrown by the {@link org.id4me} package.
 */

package org.id4me.exceptions;
