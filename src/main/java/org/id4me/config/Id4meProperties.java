/*
 * Copyright (C) 2016-2020 OX Software GmbH
 * Developed by Sven Woltmann sven.woltmann@androidpit.de
 * See the LICENSE file for licensing conditions
 * SPDX-License-Identifier: MIT
*/

package org.id4me.config;

/**
 *
 * @author Sven Woltmann
 */
public class Id4meProperties {

	private String clientName;
	private String redirectURI;
	private String[] redirectURIs;
	private String logoURI = "";
	private String dnssecRootKey;
	private boolean dnssecRequired = true; // should be set to false only for testing
	private String dnsResolver;
	private String registrationDataPath;
	private String pubKeyFile;
	private String privKeyFile;
	private Integer max_fetch_size;
	private boolean fallbackToScopes = true;
	
	public String getPubKeyFile() {
		return pubKeyFile;
	}

	public Id4meProperties setPubKeyFile(String pubKeyFile) {
		this.pubKeyFile = pubKeyFile;
		return this;
	}

	public String getPrivKeyFile() {
		return privKeyFile;
	}

	public boolean isFallbackToScopes() {
		return fallbackToScopes;
	}

	public Id4meProperties setFallbackToScopes(boolean fallbackToScopes) {
		this.fallbackToScopes = fallbackToScopes;
		return this;
	}

	public Id4meProperties setPrivKeyFile(String privKeyFile) {
		this.privKeyFile = privKeyFile;
		return this;
	}

	public String getClientName() {
		return clientName;
	}

	public Id4meProperties setClientName(String clientName) {
		this.clientName = clientName;
		return this;
	}

	public String getRedirectURI() {
		return redirectURI;
	}

	public Id4meProperties setRedirectURI(String redirectURI) {
		this.redirectURI = redirectURI;
		return this;
	}

	public String getLogoURI() {
		return logoURI;
	}

	public Id4meProperties setLogoURI(String logoURI) {
		this.logoURI = logoURI;
		return this;
	}

	public String getDnssecRootKey() {
		return dnssecRootKey;
	}

	public Id4meProperties setDnssecRootKey(String dnssecRootKey) {
		this.dnssecRootKey = dnssecRootKey;
		return this;
	}

	public boolean isDnssecRequired() {
		return dnssecRequired;
	}

	public Id4meProperties setDnssecRequired(boolean dnssecRequired) {
		this.dnssecRequired = dnssecRequired;
		return this;
	}

	public String getDnsResolver() {
		return dnsResolver;
	}

	public Id4meProperties setDnsResolver(String dnsResolver) {
		this.dnsResolver = dnsResolver;
		return this;
	}

	public String getRegistrationDataPath() {
		return registrationDataPath;
	}

	public Id4meProperties setRegistrationDataPath(String registrationDataPath) {
		this.registrationDataPath = registrationDataPath;
		return this;
	}

	public String[] getRedirectURIs() {
		if(redirectURIs == null) {
			return new String[] {redirectURI};
		}
		return redirectURIs;
	}

	public Id4meProperties setRedirectURIs(String[] redirectURIs) {
		this.redirectURIs = redirectURIs;
		return this;
	}

	public Integer getMaxFetchSize() {
		return max_fetch_size;
	}

	public Id4meProperties setMaxFetchSize(int max_fetch_size) {
		this.max_fetch_size = max_fetch_size;
		return this;
	}

}
