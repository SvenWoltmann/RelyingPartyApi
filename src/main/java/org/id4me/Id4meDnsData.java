/*
 *
 * Copyright (C) 2018 Fonpit AG
 *
 */

package org.id4me;

/**
 *
 * @author Sven Woltmann
 */
class Id4meDnsData {

	private final String v;
	private final String iau;
	private final String iag;

	Id4meDnsData(String v, String iau, String iag) {
		this.v = v;
		this.iau = iau;
		this.iag = iag;
	}

	String getV() {
		return v;
	}

	String getIau() {
		return iau;
	}

	String getIag() {
		return iag;
	}

}
