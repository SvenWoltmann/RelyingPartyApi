/*
 *
 * Copyright (C) 2018 Fonpit AG
 *
 */
package org.id4me.util;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Provides file I/O functionality.
 *
 * @author Sven Woltmann
 */
public class FileReader {

	private FileReader() {
		// hide implicit public constructor of utility class
	}

	public static String readFileToString(Path path) throws IOException {
		StringBuilder sb = new StringBuilder();
		try (Reader reader = Files.newBufferedReader(path)) {
			char[] buff = new char[8192];
			int len;
			while ((len = reader.read(buff)) > 0) {
				sb.append(buff, 0, len);
			}
		}
		return sb.toString();
	}

}
