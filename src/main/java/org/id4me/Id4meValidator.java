/*
 * Copyright (C) 2016-2020 OX Software GmbH
 * Developed by Peter Höbel peter.hoebel@open-xchange.com
 * See the LICENSE file for licensing conditions
 * SPDX-License-Identifier: MIT
*/

package org.id4me;

import java.util.regex.Pattern;

/**
 * Provides functionality to validate an ID4me identifier.
 * 
 * @author Peter Hoebel
 */
public class Id4meValidator {

	private static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern
			.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,63}$", Pattern.CASE_INSENSITIVE);

	private static final Pattern VALID_DOMAIN_NAME_REGEX = Pattern
			.compile("^((?!-)[_A-Za-z0-9-]{1,63}(?<!-)\\.)+[A-Za-z]{2,63}$");

	private static final Pattern VALID_TOP_LEVEL_DOMAIN_NAME_REGEX = Pattern
			.compile("^[A-Za-z]{2,63}$");

	/**
	 * Check whether the id4me is either a valid email address, or a valid domain
	 * name.
	 * 
	 * @param id4me
	 *            the id to logon
	 * @return true if the id4me has a valid format, false if not
	 */
	public static boolean isValidUserid(String id4me) {
		if (id4me == null || "".equals(id4me.trim()))
			return false;

		int idx = id4me.indexOf('@');
		if (idx == 0)
			return false;

		if (idx > 0) {
			return VALID_EMAIL_ADDRESS_REGEX.matcher(id4me).find();
		} else {
			if (id4me.indexOf(".") > 0)
				return VALID_DOMAIN_NAME_REGEX.matcher(id4me).find();
			else
				return VALID_TOP_LEVEL_DOMAIN_NAME_REGEX.matcher(id4me).find();
		}
	}

}
