/*
 *
 * Copyright (C) 2018 Fonpit AG
 *
 */
package org.id4me;

import org.id4me.config.Id4meClaimsParameters;
import org.id4me.config.Id4meClaimsParameters.Entry;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Sven Woltmann
 */
class Id4meClaimsConfigParser {

	private static final Logger log = LoggerFactory.getLogger(Id4meClaimsConfigParser.class);

	private Id4meClaimsConfigParser() {
		// hide implicit public constructor of utility class
	}

	static Id4meClaimsConfig parseClaimsConfigJSON(String claimsConfigJSON) {
		JSONArray claims = new JSONArray(claimsConfigJSON);

		Id4meClaimsParameters parameters = new Id4meClaimsParameters();

		for (Object o : claims) {
			Entry entry = parseSingleClaim(o);
			if (entry != null) {
				parameters.addEntry(entry);
			}
		}

		return new Id4meClaimsConfig(parameters);
	}

	private static Entry parseSingleClaim(Object o) {
		if (!(o instanceof JSONObject)) {
			log.error("Not a JSONObject: class: {}, value: {}", o.getClass().getName(), o);
			return null;
		}

		JSONObject claim = (JSONObject) o;
		if (!claim.has("name")) {
			log.error("Attribute 'name' missing in claim JSONObject: {}", claim);
			return null;
		}

		Entry entry = new Entry();
		entry.setName(claim.getString("name"));

		if (claim.has("essential")) {
			entry.setEssential(claim.getBoolean("essential"));
		}

		if (claim.has("reason")) {
			entry.setReason(claim.getString("reason"));
		}

		return entry;
	}

}
