/*
 *
 * Copyright (C) 2018 Fonpit AG
 *
 */
package org.id4me;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Arrays;
import java.util.HashSet;

import org.junit.Test;

/**
 *
 * @author Sven Woltmann
 */
public class Id4meClaimsConfigTest {

	@Test
	public void testClaimsConfig() {
		Id4meClaimsConfig claimsConfig = new Id4meClaimsConfig(Id4meLogonConfigTest.buildClaimsParams());

		assertThat(claimsConfig.getClaimsParam(), is("{\"userinfo\":{" //
				+ "\"name\":{\"reason\":\"Displayname in the user data\"}," //
				+ "\"given_name\":null,"
				+ "\"email\":{\"reason\":\"Needed to create the profile\",\"essential\":true}}}"));

		assertThat(claimsConfig.getEssentialClaims(), is(new HashSet<>(Arrays.asList("email"))));

		assertThat(claimsConfig.isEssential("name"), is(false));
		assertThat(claimsConfig.isEssential("given_name"), is(false));
		assertThat(claimsConfig.isEssential("email"), is(true));
		assertThat(claimsConfig.isEssential("unknown"), is(false));
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testEssentialClaimsIsUnmodifiable() {
		Id4meClaimsConfig claimsConfig = new Id4meClaimsConfig(Id4meLogonConfigTest.buildClaimsParams());
		claimsConfig.getEssentialClaims().add("new");
	}

}
