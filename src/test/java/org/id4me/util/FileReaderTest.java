/*
 *
 * Copyright (C) 2018 Fonpit AG
 *
 */
package org.id4me.util;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.lang.reflect.Method;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;

/**
 *
 * @author Sven Woltmann
 */
public class FileReaderTest {

	private static final String EXPECTED_RESPONSE = "Test file without special characters as we use the System's default character set to read it";

	@Test
	public void testReadFile() throws Exception {
		Path path = Paths.get(this.getClass().getClassLoader().getResource("test.txt").toURI());

		Method method = FileReader.class.getDeclaredMethod("readFileToString", Path.class);
		method.setAccessible(true);
		String test = (String) method.invoke(null, path);

		assertThat(test, is(EXPECTED_RESPONSE));
	}

}
